# Bylkov Maksim
## My code examples

* [SPA API search](https://bitbucket.org/fear-cry/react-demo-app/src/master/)
React, Redux, Routing, custom webpack config,
API requests by axios, testing by Jest and RTL.


* [Chat API](https://github.com/attikos/adonisjs-chat-api)
AdonisJS, PostgreSQL, Websocket, Auth.

* [Chat Frontend](https://github.com/attikos/adonisjs-chat-frontend)
VueJS, Websocket, Auth.
